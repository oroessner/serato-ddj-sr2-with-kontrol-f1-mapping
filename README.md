# Serato DDJ-SR2 with Kontrol F1 Mapping

I created this mapping, since the DDJ-SR2 is the best controller I know for Serato DJ, but it lacks two features I'd get used to with my previous controller.

1. As I mostly play with "Simple Sync", I want a button to toggle "Quant-Mode" here and there.
2. I miss a way to directly place loop of a fixed sice, without fiddeling around with the loop size buttons.

If you know a build-in way to activate any of the features above, please let me know!

## Installation

* Close Serato DJ if running!
* Make sure the KONTROL F1 is connected via USB
* Open NI Controller Editor
* Import [Serato DJ - DDJ + F1.ncc](NI Controller Editor/Serato DJ - DDJ + F1.ncc) via "File -> Open Template..."
* Copy [DDJ-SR2 + F1 for Loops.xml](Serato DJ/DDJ-SR2 + F1 for Loops.xml) to your `_Serato_` Folder under `_Serato_/MIDI/Xml`. The `_Serato_` is in your users main Music folder (e.g. `~/Music` on OSX or `C:\Users\<your-username>\Music` on Windows) or (if you are playing from an USB Drive) on the root level of the external drive.
* Start Serato DJ, go to Setup -> MIDI, search for `DDJ-SR2 + F1 for Loops`, select it and click on the "Load" button. Make sure "Enable Output Lightning" is checked!
* Have Fun!

## Mappings

* Quant => Quant

### Perfomance Pads

| Deck A | | Deck B | |
| --- | --- | --- | --- |
| Loop 1/8 | Loop 2 | Loop 1/8 | Loop 1 |
| Loop 1/4 | Loop 4 | Loop 1/4 | Loop 4 |
| Loop 1/2 | Loop 8 | Loop 1/2 | Loop 8 |
| Loop 1 | Loop 16 | Loop 1 | Loop 16 |

# Contact Me

* [Twitter](https://twitter.com/djbasster)
* [Mixcloud](https://www.mixcloud.com/basster/)
